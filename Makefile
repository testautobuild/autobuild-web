
webdir = web

template_SOURCES = $(shell find -name '*.htmlt')
template_DATA = $(template_SOURCES:./%.htmlt=build/%.html)

extra_SOURCES = $(wildcard screenshots/*.png) $(wildcard architecture/*.png) $(wildcard styles/*.css) $(wildcard styles/*.png) $(wildcard styles/*.jpeg)
extra_DATA = $(extra_SOURCES:%=build/%)

all: $(template_DATA) $(extra_DATA)

build/%.html: %.htmlt lib/page.tt
	mkdir -p `dirname $@`
	bin/ttmake -I lib -I faqs -I . $< $@

build/%: %
	mkdir -p `dirname $@`
	cp -f $< $@

install: $(template_DATA) $(extra_DATA)
	mkdir -p $(webdir)
	@for i in $(template_DATA) ; \
	do \
		echo $$i ; \
		install $$i $(webdir)/$$i; \
	done
	@for i in $(extra_DATA) ; \
	do \
		echo $$i ; \
		install $$i $(webdir)/$$i; \
	done

clean:
	rm -rf build
