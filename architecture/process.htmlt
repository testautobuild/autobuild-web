[% WRAPPER page.tt 
    title = "Build process"
    url = "architecture/process.html"
    path = ".." %]

  <p class="diagram">
  The diagram below and the description that follows outline the tasks
  performed on each run of the auto build.
  <br/>
  <img src="process.png" alt="Process diagram">
  </p>
  
  <h2>Initialize</h2>
  
  <ol>
  <li>The master configuration file (specified with the --config command line
  option) is processed and used to create the various Perl objects required for
  the build</li>
  <li>An exclusive lock is aquired to prevent multiple instances of the builder
  running concurrently. If the lock can't be aquired the builder exits silently.
  This allows a simple crontab entry to run the builder every 5 minutes - it merely
  exits if its already running.</li>
  <li>The priority of the build process is lowered to help ensure that the build
  machine remains relatively responsive even when under heavy load.</li>
  <li>Global environment variables specified in the config file are set to
  have effect for the remainder of the build run.</li>
  </ol>

  <h2>Checkout</h2>
  
  <ol>
  <li>Initialize each repository with any global configuration such
  as environment variables, or in the case of Perforce, build the 
  client view.</li>
  <li>If the module is new, then checkout the sources. If the module
  already exists, then update the sources (if required).
  </li>
  <li>If the module was new / changed, then clear out the build
  cache for that module
  </li>
  </ol>
  
  <h2>Build</h2>

  <ol>
  <li>Take a snapshot of the virtual install root for the
  module.
  <li>Invoke the control file to perform the build.
  <li>Detect any new packages in the various package
  spool directories
  <li>Populate the cache with changed/new files from the
  virtual install root, and with generated packages.
  </ol>
  
  <h2>Output</h2>

  <ol>
  <li>Copy the generated packages to an output directory,
  for example, for a web or FTP site.
  <li>Save the build log files to an output directory.
  <li>Generated HTML status pages listing modules built
  and their corresponding packages and log files.
  <li>Send email alerts to the build maintainer and / or
  development team upon both success and failure.
  <li>Generate ISO images containing a bundle of packages.
  </ol>

[% END %]
