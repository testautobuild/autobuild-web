[% WRAPPER faq_entry.tt %]
[% title = "How do I clean up build state" %]

<p>
If a previously successful build begins failing for no
apparent reason, or if there was a transient failure in
the underlying operating system (disk full, network down,
etc), some of the builder's state may become corrupt. The
first task in such a situation is to delete the entire
contents of the build cache. This is typically located in
a directory named <code>build-cache</code> within the
build home
</p>

<pre class="shell">
$ su - builder
# cd /var/builder
# rm -rf build-cache/*
</pre>

<p>
Sometimes the actual source code checkout for a module
becomes corrupt (typically either due to broken build 
scripts / makefiles which change source code during the 
build process, or transient operating system failures)
Thus if cleaning out the build-cache did not solve the
failure, then the next step is to delete the contents of
the build root (in 1.0.x releases called <code>build-home</code>, 
renamed to <code>build-root</code> in 1.1.x releases).
</p>

<pre class="shell">
$ su - builder
# cd /var/builder
# rm -rf build-home/*
</pre>


[% END %]