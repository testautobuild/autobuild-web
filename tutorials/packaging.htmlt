[% WRAPPER page.tt 
    title = "Tutorial: Packaging"
    url = "tutorials/packaging.html"
    path = ".." %]

<p>
An increasingly important part of building and deploying software
is the creation of packages in formats such as RPM, Debian or 
simple ZIP / Tar+GZip. Rather than explain how to create packages
in these formats, this page outlines some of the issues specific
to integrating with the autobuild framework.
</p>

<h2>RPM Integration</h2>

<p>
Since the autobuilds run as an unprivileged user (<code>builder</code>)
it is neccessary to create and configure a local RPM build root. The
example autobuild configuration file expects the RPM build root to be
in the directory <code>/var/builder/packages/rpm</code>. To create this,
login as the user 'builder' and run
</p>

<pre class="shell">
cd /var/builder
mkdir packages
mkdir packages/rpm
mkdir packages/rpm/{RPMS,SRPMS,SPECS,SOURCES,BUILD}
mkdir packages/rpm/RPMS/{i386,i486,i586,i686,sparc,noarch}
</pre>

<p>
To instruct RPM to use this build root is it neccessary to create
a <code>.rpmmacros</code> file containing:
</p>

<pre class="code">
%_topdir /var/builder/packages/rpm
</pre>

<h2>Debian Integration</h2>

<p>
The Debian packaging tools don't need a central build root to 
operate with - instead they create files in a subdir of the
package being built. The autobuild package detection does,
however, expect the resulting binary packages to be placed in
a central directory - by convention <code>/var/builder/packages/debian</code>.
This is accomplished by specifying the <code>DESTDIR</code>
variable when running the main build command:
</p>

<pre class="shell">
fakeroot debian/rules binary DESTDIR=/var/builder/packages/debian
</pre>

<h2>ZIP / Tar+GZip Integration</h2>

<p>
As with Debian packaging, there is no need for a centralized 
build root. It is merely neccessary to put the resulting ZIP
or Tar+GZip binary files in a central distribution directory -
by convention <code>/var/builder/packages/zip</code> or
<code>/var/builder/packages/tgz</code>.

[% END %]
