<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
  <title>Test-AutoBuild: [% title %]</title>

  [% DEFAULT keywords = "autobuild, test-autobuild, automated, software, builds, builder, continuous, unattended" %]
  [% DEFAULT description = "Perl framework for performing continuous, unattended, automated software builds" %]
  [% DEFAULT path = "." %]

  <meta name="keywords" content="[% keywords %]">
  <meta name="description" content="[% description %]">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <link rel="StyleSheet" type="text/css" href="[%path%]/styles/default.css" title="Main style">
  <link rel="Alternate StyleSheet" type="text/css" href="[%path%]/styles/default-debug.css" title="Debug main style">
</head>

<body>
  <div id="header">
    <h1><a href="[% IF path %][% path %]/[% END %]index.html">Test-AutoBuild</a>[%IF title %]: [% title %][% END %]</h1>

    <div id="tagline">
      <p>The Perl framework for performing continuous, unattended, automated software builds</p>
    </div>
  </div>

  <div id="main">
    <div id="content">
    [% content %]
    </div>
  </div>

  <div id="panel">
    <div id="navigation">
       <div class="section">

	 <h3 id="download"><a href="download.html">Download</a></h3>

	 <p>
	   Install it from your Linux distribution (others coming soon)
	 </p>

	 <pre># yum install perl-Test-AutoBuild (Fedora)</pre>

	 <p>
	   Or grab the <a href="download.html">source release</a>
	 </p>
       </div>

       <div class="section">
	 <h3 id="comms"><a href="https://gna.org/mail/?group=testautobuild">Communicate</a></h3>

	 <p>
	   Join the <a href="https://gna.org/mail/?group=testautobuild">testautobuild-devel</a> mailing list
	 </p>
       </div>

       <div class="section">
	 <h3 id="docs"><a href="docs.html">Documentation</a></h3>

	 <p>
	   Read <a href="docs.html#tutorials">tutorials</a>,
	   learn about the <a href="docs.html#architecture">architecture</a>,
	   or read the <a href="faq.html">FAQs</a>
	 </p>
       </div>

       <div class="section">
	 <h3 id="bugs"><a href="https://gna.org/projects/testautobuild/">Project bugs/todo</a></h3>

	 <p>
	   View <a href="https://gna.org/bugs/?group=testautobuild">known</a> bugs
	 </p>
	 <p>
	   View <a href="https://gna.org/task/?group=testautobuild">todo</a> items
	 </p>
       </div>

       <div class="section">
	 <h3 id="scm"><a href="scm.html">Code repository</a></h3>

	 <p>
	   The code is <a href="http://gitorious.org/testautobuild/testautobuild">browsable</a> online.
	 </p>

	 <p>
	   Get a personal checkout from GIT
	 </p>
	 <pre style="font-size: smaller"># git clone \
	   git://gitorious.org/testautobuild/testautobuild.git</pre>
       </div>
    </div>
  </div>

  <div id="footer">
  <p>
    &copy; 2000-2011 Daniel P. Berrang&eacute; and other contributors. Website design <a href="[% path %]/credits.html">credits & license</a> info.
  </p>
  </div>

</body>
</html>
